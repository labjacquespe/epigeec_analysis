from unittest import TestCase
from mock import patch, MagicMock
from hypothesis import given
from hypothesis import strategies as st
from analysis.annotate.report.composers.composer import Composer, LegendMaker


class TestComposer(TestCase):
    def test_run(self):
        with self.assertRaises(NotImplementedError):
            Composer().run(None, None, None)

    @patch('os.remove')
    @patch('os.close')
    def test_tmp_name(self, close, remove):
        mkstemp_fd = MagicMock()
        mkstemp_temp_path = MagicMock(spec=str)

        with patch('tempfile.mkstemp',
                   return_value=(mkstemp_fd, mkstemp_temp_path)) as mkstemp:
            temp_path = Composer().tmp_name()
            mkstemp.assert_called()

        close.assert_called_with(mkstemp_fd)
        remove.assert_called_with(temp_path)
        self.assertIs(temp_path, mkstemp_temp_path)


class TestLegendMaker(TestCase):
    @given(st.lists(st.text()))
    def test_create_colors_dict_property(self, labels):
        LegendMaker.create_colors_dict(labels)
