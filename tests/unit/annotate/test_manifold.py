from unittest import TestCase
from analysis.manifold import MDSManifoldFactory, UMAPManifoldFactory
import sklearn.manifold as sm
import umap

RANDOM_STATE = 3


class TestUMAPManifoldFactory(TestCase):
    def test_make(self):
        factory = UMAPManifoldFactory(RANDOM_STATE)
        manifold = factory.make()
        self.assertIsInstance(manifold, umap.UMAP)


class TestMDSManifoldFactory(TestCase):
    def test_make(self):
        factory = MDSManifoldFactory(RANDOM_STATE)
        manifold = factory.make()
        self.assertIsInstance(manifold, sm.MDS)
