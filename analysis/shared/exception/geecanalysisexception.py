class GeecAnalysisException(Exception):
    '''
    This is for critical failure.
    '''
    pass
